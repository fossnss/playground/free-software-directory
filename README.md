# Free Software Directory

Free Software Directory project is created as a part of the one-day git workshop **Codesync: Git Gathering** on 1st December, 2023, conducted by **FOSSNSS, NSS College of Engineering, Palakkad**.

# Preview

This project is served using gitlab pages and can be accessed using the following URL:

https://fossnss.gitlab.io/playground/free-software-directory

# Credits

[Mountain - Website template from Theme Wagon](https://themewagon.com/themes/free-bootstrap-agency-html5-template-download-mountain)

# License

See the Free Themes section at https://themewagon.com/license.

All contents of the website are licensed under [CC-BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0) unleess otherwise specified.
